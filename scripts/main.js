$(function() {

	//header interaction
	$(window).on('scroll', function() {
		if ($(document).scrollTop() < 30) {
			$('body').removeClass('smallHeader');	
		} else {
			$('body').addClass('smallHeader');
		}
	});

	//footer interaction
	// $('.js-openfooter').on('click', function(e) {
	// 	e.preventDefault();
	// 	$('footer').toggleClass('opened');
	// });

	//event switch
	$('.eventSwitch').on('click', function(e) {
		e.preventDefault();
		$('body').toggleClass('eventOpened');
	});


    //aside menu
    var asideAcc = new Accordion('.asideAcc', 'a', 'div');
    asideAcc.init();

    //pdcontent
    // var pdAcc = new Accordion('.pdAcc', 'a', 'div');
    // pdAcc.init();


    $('.js-mfp').magnificPopup();
});

function Accordion(el, head, cnt) {
    var self = this;
    self.parent = $(el);
    self.head = $(el + ' > ' + head);
    self.content = self.head.next(cnt);
    self.animating = false;
    self.init = function() {
    	self.parent.addClass('accordion');
    	self.head.each(function() {
    		$(this).addClass('accordion-head');
    	});
        self.content.each(function() {
            var originHeight = $(this).height();
            $(this).addClass('accordion-content');
            $(this).data('cntheight', originHeight);
            $(this).css({height: 0, opacity: 1});
        });
        self.bindEvent();
    };

    self.bindEvent = function() {
        self.head.on('click', function(e) {
            e.preventDefault();
            if (self.animating) {
                return;
            }
            self.animating = true;
            if ($(e.target).hasClass('active')) {
                //start closing
                $.when( self.closing($(e.target), $(e.target).next(cnt)) ).done(function() {
                        $(e.target).removeClass('animating').removeClass('active'); //closed
                        self.animating = false;
                });
            } else {
                //start opening
                var siblingsHead = $(e.target).siblings(head);
                var targetHeight = $(e.target).next(cnt).data('cntheight');
                $.when( self.opening($(e.target), targetHeight), self.siblingsClosing(siblingsHead) ).done(function() {
                        $(e.target).removeClass('animating').addClass('active');
                        self.animating = false;
                });
            }
        });

    };

    self.closing = function(targetHead, targetCnt) {
        targetHead.addClass('animating');
        return targetCnt.removeClass('active').animate({height: 0}, 300, 'linear');
    };

    self.opening = function(targetHead, targetHeight) {
        targetHead.addClass('animating');
        return targetHead.next(cnt).addClass('active').animate({height: targetHeight}, 300, 'linear');
    };

    self.siblingsClosing = function(siblings) {
        siblings.removeClass('active');
        return siblings.next(cnt).removeClass('active').animate({height: 0}, 300, 'linear');
    }
}

